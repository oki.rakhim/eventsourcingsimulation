import BaseEvent from "../events/BaseEvent/BaseEvent";

interface IEventStore {
    storeNewEvent(event: BaseEvent):void;
    resetStore():void;
    getAllEvents():any;
}

export default IEventStore;