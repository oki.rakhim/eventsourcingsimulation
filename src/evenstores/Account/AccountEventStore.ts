import fs from 'fs';
import path from 'path';
import os from 'os';
import _ from 'lodash';
import { EVENT_LOG_PATH } from './AccountEventStoreConfig';
import IEventStore from '../IEventStore';
import { AccountEvent, AccountEventType } from '../../events/Account/AccountEvent';
import AccountRepo from '../../repositories/Account/AccountRepo';
import Account from '../../domains/Account/Account';
import AccountState from '../../events/Account/AccountState';
import AccountSubscriber from '../../subscriber/Account/AccountSubscriber';


class AccountEventStore implements IEventStore {
    
    private subscriber:AccountSubscriber; //for simulation purpose, it pretends as event emitter, as if there was a subscriber 

    constructor(subscriber:AccountSubscriber){
        this.subscriber = subscriber;
    }

    storeNewEvent(accountEvent: AccountEvent):void{


        const adjustedAccountState = accountEvent.getState();
        if (adjustedAccountState.getType() === AccountEventType.EVENT_TYPE.CLOSE){
            const account = AccountRepo.getAccount(adjustedAccountState.getName());
            if (account){
                let newClosedState =   AccountState.create(adjustedAccountState.getName(),AccountEventType.EVENT_TYPE.CLOSE,account.getBalance());
                fs.appendFileSync(EVENT_LOG_PATH, JSON.stringify(newClosedState) + os.EOL);
                
            } else
            throw new Error("Account does not exist.");
        } else {
            fs.appendFileSync(EVENT_LOG_PATH, JSON.stringify(accountEvent.getState()) + os.EOL);
        }

        this.subscriber.handleEvent(accountEvent);
            
    }

    resetStore():void{

        fs.writeFileSync(EVENT_LOG_PATH, '');
    }

    

    getAllEvents():any{

        const eventLines = fs.readFileSync(EVENT_LOG_PATH, 'utf-8')

        return eventLines
        .split(os.EOL)
        .filter((eventLineStr) => eventLineStr.length)
        .map((eventLineStr) => {
            let eventLine = {}
            try {
                eventLine = JSON.parse(eventLineStr)
            } catch (err) {
                console.error(err)
            }
            return eventLine
        })
    }
}


export default AccountEventStore;