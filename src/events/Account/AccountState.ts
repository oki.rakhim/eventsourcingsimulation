import { randomUUID } from "crypto";
import BaseState from "../BaseEvent/BaseState";
import { AccountEventType } from "./AccountEvent";

class AccountState extends BaseState{

    private readonly type!:string;
    private readonly balance?: number;
    private readonly name!:string;
    private readonly fromName?: string;
    private readonly toName?:string;
    private readonly amount?:number;
    

    private constructor (name:string, type: string, balance?: number, fromName?: string, toName?: string, amount?: number) {
        super();
        this.name = name;
        this.type = type;

        if (balance) this.balance = balance;
        if (fromName) this.fromName = fromName;
        if (toName) this.toName = toName;
        if (amount) this.amount = amount;

    } 
    
   
    static create(name:string, type: string, balance?: number|undefined,fromName?: string, toName?: string, amount?: number): AccountState {
        
        if (type===AccountEventType.EVENT_TYPE.TRANSFER)
            return new AccountState(name, type, undefined, fromName, toName, amount );

        return new AccountState(name, type, balance);  
    }
   
    getBalance():number{
        if (this.balance)
            return this.balance;

        return 0;
    }

    getName(){
        return this.name;
    }

    getType(){
        return this.type;
    }

    getFromName(){
        if (this.fromName)
        return this.fromName;
    }

    getToName(){
        if (this.toName)
        return this.toName;
    }

    getAmount(){
        if (this.amount)
        return this.amount;
    }

}

export default AccountState;