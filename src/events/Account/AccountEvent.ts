
import AccountState from "./AccountState";
import AccountEventStore from '../../evenstores/Account/AccountEventStore';
import IEventStore from "../../evenstores/IEventStore";
import BaseEvent from "../BaseEvent/BaseEvent";
import AccountRepo from "../../repositories/Account/AccountRepo";
import Account from "../../domains/Account/Account";

class AccountEventType {

    static EVENT_TYPE = {
        OPEN: 'open',
        CLOSE: 'close',
        TRANSFER: 'transfer'
    }
}

class AccountEvent extends BaseEvent{

    private readonly accountState: AccountState;
    private eventStore: IEventStore;

    constructor(eventStore: IEventStore, accountState: AccountState){
        super();
        this.eventStore = eventStore;
        this.accountState = accountState;
    }

    static create(eventStore: IEventStore, accountState: AccountState):AccountEvent{
        return new AccountEvent(eventStore,accountState);
    }


    getState(): AccountState{
        return this.accountState;
    }

    
}


export  {AccountEvent, AccountEventType};
