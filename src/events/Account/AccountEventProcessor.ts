import Account from "../../domains/Account/Account";
import IEventStore from "../../evenstores/IEventStore";
import AccountRepo from "../../repositories/Account/AccountRepo";
import BaseEventProcessor from "../BaseEvent/BaseEventProcessor";
import { AccountEvent, AccountEventType } from "./AccountEvent";
import AccountState from "./AccountState";

class AccountEventProcessor extends BaseEventProcessor{

     
    constructor(eventStore: IEventStore){
        super(eventStore);
    }
    
    append(payload:any): void {
        console.log("-> new event is stored");
        
        const accountState = AccountState.create(payload.name,payload.type, payload.balance, payload.from, payload.to,payload.amount);

        const accountEvent = AccountEvent.create(this.getEventStore() , accountState);

        this.getEventStore().storeNewEvent(accountEvent);
    }


    reset(): void {
        this.getEventStore().resetStore();
    }

    getAllEvents(): any {
         return this.getEventStore().getAllEvents();
    }

    rebuild(steps: number): void {
        let events = this.getAllEvents()

        events = events.splice(0, steps)
        
        return this._build(events); 
    }

    rebuildAll(): void {
        let events = this.getAllEvents()
        
        return this._build(events); 
    }

    private _build(events: any) {
        AccountRepo.reset();

        return events
            .map(( event: any) => {
                    
                if (event.type === AccountEventType.EVENT_TYPE.OPEN) {
                    AccountRepo.addNewAccount(new Account(event.id, event.name, event.balance));
                } else if (event.type === AccountEventType.EVENT_TYPE.CLOSE) {
                    AccountRepo.deleteAccount(event.name);
                } else if (event.type === AccountEventType.EVENT_TYPE.TRANSFER) {
                    AccountRepo.updateAccount(event.fromName, event.amount * -1);
                    AccountRepo.updateAccount(event.toName, event.amount);
                }
            });
    }

    

    undo(steps: number): void {
        let events = this.getAllEvents()

        events = events.splice(-steps)

        return events
            .reduceRight((accounts:any, event:any) => {

                if (event.type === AccountEventType.EVENT_TYPE.OPEN) {
                    AccountRepo.deleteAccount(event.name);
                } else if (event.type === AccountEventType.EVENT_TYPE.CLOSE) {
                    console.log("get in here");
                    AccountRepo.addNewAccount(new Account(event.id, event.name, event.balance));
                } else if (event.type === AccountEventType.EVENT_TYPE.TRANSFER) {
                    AccountRepo.updateAccount(event.fromName, event.amount);
                    AccountRepo.updateAccount(event.toName, event.amount * -1);
                }
            },{}) 
    }

}


export default AccountEventProcessor