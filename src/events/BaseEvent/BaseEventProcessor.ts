import IEventStore from "../../evenstores/IEventStore";
import BaseEvent from "./BaseEvent";

abstract class  BaseEventProcessor {

    private eventStore: IEventStore; 

    constructor(eventStore: IEventStore){
        this.eventStore = eventStore;
    }

    abstract append(payload:any):void;
    abstract getAllEvents(): void;
    abstract rebuild(steps:number): void;
    abstract rebuildAll(): void;
    abstract undo(steps:number): void;
    abstract reset(): void;
    
    getEventStore():IEventStore{
        return this.eventStore;
    }
}

export default BaseEventProcessor;