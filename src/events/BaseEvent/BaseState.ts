import { randomUUID } from "crypto";

abstract class  BaseState {
    private timestamp : number;
    private id: string;

    constructor() {
        this.timestamp = Date.now();
        this.id = randomUUID();
    }

    getId():string{
        return this.id;
    }
}

export default BaseState;