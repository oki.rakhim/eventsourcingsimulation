
import AccountCommand from "./commands/Account/AccountCommand";
import AccountEventStore from "./evenstores/Account/AccountEventStore";
import AccountEventProcessor from "./events/Account/AccountEventProcessor";
import Helper from "./helper/helper";
import BaseEventProcessor from "./events/BaseEvent/BaseEventProcessor";
import AccountSubscriber from "./subscriber/Account/AccountSubscriber";

const accountSubscriber : AccountSubscriber = new AccountSubscriber();

const eventStore: AccountEventStore = new AccountEventStore(accountSubscriber);

const accountEventProcessor: BaseEventProcessor= new AccountEventProcessor(eventStore);

const accountCommand: AccountCommand = new AccountCommand(accountEventProcessor);





console.log(' ');
console.log('================= RESET EVENT STORE');
accountEventProcessor.reset();

console.log(' ');
console.log('================= OPEN ACCOUNTS');
console.log(' ');

accountCommand.openAccount("oki",2000);
accountCommand.openAccount("idam",1000);
accountCommand.openAccount("alev",5000);

console.log(' ');
console.log('================= DISPLAY CURRENTS ACCOUNTS FROM THE DATABASE');
console.log(' ');

Helper.displayData(AccountCommand.getCurrentAccounts());

console.log(' ');
console.log('================= TRANSFER MONEY');
console.log(' ');

accountCommand.transferMoney("oki","idam",500);
accountCommand.transferMoney("oki","alev",700);

console.log(' ');
console.log('================= DISPLAY CURRENTS ACCOUNTS FROM THE DATABASE');
console.log(' ');

Helper.displayData(AccountCommand.getCurrentAccounts());

console.log(' ');
console.log('================= CLOSE ACCOUNT: OKI');
console.log(' ');

accountCommand.closeAccount("oki");


console.log(' ');
console.log('================= DISPLAY CURRENTS ACCOUNTS FROM THE DATABASE');
console.log(' ');

Helper.displayData(AccountCommand.getCurrentAccounts());

/* console.log(' ');
console.log('================= DELETE ALL ACCOUNTS FROM THE DATABASE');
console.log(' ');
AccountCommand.deleteAllAccounts();

console.log(' ');
console.log('================= DISPLAY CURRENTS ACCOUNTS FROM THE DATABASE');
console.log(' ');

currentAccounts = AccountCommand.getCurrentAccounts();
currentAccounts.map(account => console.log(JSON.stringify(account))); */

console.log(' ');
console.log(' ');
console.log('=======  EVENT SOURCING - CAPABILITIES ================================== ');


console.log(' ');
console.log('================= UNDO 3 STEPS');
console.log(' ');

accountEventProcessor.undo(3);

console.log(' ');
console.log('================= DISPLAY CURRENTS ACCOUNTS FROM THE DATABASE');
console.log(' ');

Helper.displayData(AccountCommand.getCurrentAccounts());

console.log(' ');
console.log('================= REBUILD 5 STEPS');
console.log(' ');

accountEventProcessor.rebuild(5);


console.log(' ');
console.log('================= DISPLAY CURRENTS ACCOUNTS FROM THE DATABASE');
console.log(' ');

Helper.displayData(AccountCommand.getCurrentAccounts());