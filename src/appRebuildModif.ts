
import AccountCommand from "./commands/Account/AccountCommand";
import AccountEventStore from "./evenstores/Account/AccountEventStore";
import AccountEventProcessor from "./events/Account/AccountEventProcessor";
import Helper from "./helper/helper";
import AccountSubscriber from "./subscriber/Account/AccountSubscriber";

const accountSubscriber : AccountSubscriber = new AccountSubscriber();
const eventStore: AccountEventStore = new AccountEventStore(accountSubscriber);

const accountEventProcessor: AccountEventProcessor = new AccountEventProcessor(eventStore);

console.log(' ');
console.log('================= DISPLAY CURRENTS ACCOUNTS FROM THE DATABASE');
console.log(' ');

Helper.displayData(AccountCommand.getCurrentAccounts());

console.log(' ');
console.log('================= rebuilding all account data from event log...');
console.log(' ');
accountEventProcessor.rebuildAll();

console.log(' ');
console.log('================= DISPLAY CURRENTS ACCOUNTS FROM THE DATABASE');
console.log(' ');

Helper.displayData(AccountCommand.getCurrentAccounts());