import Account from "../../domains/Account/Account";
import { AccountEvent, AccountEventType } from "../../events/Account/AccountEvent";
import AccountRepo from "../../repositories/Account/AccountRepo";

class AccountSubscriber {

    handleEvent(accountEvent: AccountEvent){
        if (accountEvent.getState().getType()===AccountEventType.EVENT_TYPE.OPEN){
            
            AccountRepo.addNewAccount(Account.create(accountEvent.getState().getId(), accountEvent.getState().getName(), accountEvent.getState().getBalance()));
            return;
        }
        if (accountEvent.getState().getType()===AccountEventType.EVENT_TYPE.CLOSE){

            const name = accountEvent.getState().getName();
            const foundAccount = AccountRepo.getAccount(name);

            if (foundAccount){
                AccountRepo.deleteAccount(name);
                return;
            }
            throw new Error("Account does not exist.");
        }
        if (accountEvent.getState().getType()===AccountEventType.EVENT_TYPE.TRANSFER){

            const fromName = accountEvent.getState().getFromName();
            const toName = accountEvent.getState().getToName();
            const amount = accountEvent.getState().getAmount();

            const foundOriginAccount = AccountRepo.getAccount(fromName!);
            const foundTargetedAccount = AccountRepo.getAccount(toName!);

            if (foundOriginAccount && foundTargetedAccount){


                AccountRepo.updateAccount(fromName!, amount! * -1);
                AccountRepo.updateAccount(toName!, amount!);

                return
            }

            throw new Error("One or more account does not exist.");
        }
    }
}

export default AccountSubscriber;