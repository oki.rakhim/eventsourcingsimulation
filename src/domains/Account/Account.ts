class Account {
    private name: string;
    private id: string;
    private balance: number;

    constructor(id:string, name: string, balance: number){
        this.id = id;
        this.name =name;
        this.balance = balance;
    }

    static create(id:string, name: string, balance: number): Account {
        return new Account(id,name,balance);
    }

    updateBalance(balance: number){
        this.balance = balance;
    }

    getBalance(){
        return this.balance;
    }

    getId(){
        return this.id;
    }

    getName(){
        return this.name;
    }

    
}


export default Account;