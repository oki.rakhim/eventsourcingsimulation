import Account from "../../domains/Account/Account";

let currentAccounts: Account[] = [];

class AccountRepo {


    static accounts: Account[]=[];

    static reset(){
        this.accounts=[];
    }

    static addNewAccount(newAccount: Account){
        // uncomment this very line below when rebuilding with restroactive changes
        //newAccount.updateBalance(newAccount.getBalance()+100);
        this.accounts.push(newAccount);
    }

    static getAccount(name: string):Account|undefined{

        const foundAccount = this.accounts.find((account)=>
            account.getName() === name 
        );

        return foundAccount;
    }

    static deleteAccount(name: string){
        const foundAccountIndex = this.accounts.findIndex((account)=>
            account.getName() === name 
        );

        if (foundAccountIndex>-1){
            this.accounts.splice(foundAccountIndex,1);
            return;
        }

        
    }

    static updateAccount(name: string, amount: number) {
        const foundAccount = this.accounts.find((account)=>
            account.getName() === name  
        );

        if (foundAccount) {
            const newBalance = foundAccount.getBalance()+amount;
            foundAccount.updateBalance(newBalance);
        }
    }
}


export default AccountRepo;