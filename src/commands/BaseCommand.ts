import IEventStore from "../evenstores/IEventStore";
import BaseEventProcessor from "../events/BaseEvent/BaseEventProcessor";


class BaseCommand {

    //private eventStore: IEventStore;
    private eventProcessor: BaseEventProcessor;
    
    constructor(eventProcessor: BaseEventProcessor){
        //this.eventStore = eventProcessor.getEventStore();
        this.eventProcessor = eventProcessor;
    }

    //getEventStore(){
        //return this.eventStore;
    //}

    getEventProcessor(){
        return this.eventProcessor;
    }

}


export default BaseCommand;