import AccountRepo from "../../repositories/Account/AccountRepo";
import BaseCommand from "../BaseCommand";
import AccountCommandPayload from "./AccountCommandPayload";


class AccountCommand extends BaseCommand {
    
    openAccount(name: string, balance: number ):void {
        console.log(`open account: ${name}, ${balance} `);
        
        this.getEventProcessor().append(AccountCommandPayload.OpenAccount(name,balance));
    } 

    closeAccount(name: string):void {

        console.log(`close account: ${name}  `);

        this.getEventProcessor().append(AccountCommandPayload.CloseAccount(name));
    } 

    transferMoney(originName:string , targetedName:string, amount: number) {

        console.log(`transfer money : ${originName} to ${targetedName} ${amount}`);

        this.getEventProcessor().append(AccountCommandPayload.TransferMoney(originName,targetedName,amount));

    }


    static getCurrentAccounts()  {
        return AccountRepo.accounts;//put here just for simulation
    } 

    
}


export default AccountCommand;