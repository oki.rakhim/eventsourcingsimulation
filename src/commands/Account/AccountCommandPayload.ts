import { AccountEventType } from "../../events/Account/AccountEvent";

class AccountCommandPayload {

    static OpenAccount(name: string, balance: number ){
        return {name:name, balance:balance, type: AccountEventType.EVENT_TYPE.OPEN};
    }

    static CloseAccount(name: string){
        return {name:name, type:AccountEventType.EVENT_TYPE.CLOSE};
    }

    static TransferMoney(from: string, to:string, amount: number ){
        return {from:from, to:to,  amount:amount, type:AccountEventType.EVENT_TYPE.TRANSFER};
    }

}


export default AccountCommandPayload;