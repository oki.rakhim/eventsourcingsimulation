# EVENT SOURCING SIMULATION

## PURPOSE
To understand the concept of event-sourcing thru codes

## DISCLAIMER

* Only for simulation purpose, simple enough to grasp the basic ideas
* Not built under event-driven architecture, just pretend to
* Not CQRS
* Not for real production level
* Some codes break the clean code rules


### PRESENTATION LINK

[PRESENTATION](https://docs.google.com/presentation/d/1e7tZTgQX5544QbOsmrCgbYdKN1zAJOULCnfXoeNVkps/edit?usp=sharing)


## HOW

### INSTALATION

`npm install`

### RUN


1. `npm run start`

to simulate event sourcing


2. `npm run rebuild`

to rebuild with retroactive changes, uncomment line 15 in file AccountRepo.ts
